/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Research Intern
 * Yahoo! Research Barcelona
 * Spain
 */
package jvmheap;

/**
 *
 * @author Muhammad Ahsan
 */
public class JVMHeap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Get the jvm heap size.
        long heapSize = Runtime.getRuntime().totalMemory();
        //Print the jvm heap size.
        System.out.println("Heap Size in MB= " + heapSize);
    }
}
